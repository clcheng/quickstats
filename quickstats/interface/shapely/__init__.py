from quickstats.core.modules import require_module_version
require_module_version("shapely", (2, 0, 0))

from .core import *